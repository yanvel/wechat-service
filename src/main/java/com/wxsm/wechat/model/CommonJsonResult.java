package com.wxsm.wechat.model;

import com.wxsm.wechat.core.exception.ExceptionEnum;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
@Data
public class CommonJsonResult {

    private Boolean success;
    private String code;
    private String message;
    private Object data;

    private CommonJsonResult(Boolean success, String code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }

    public static CommonJsonResult success() {
        return new CommonJsonResult(true, "200", "操作成功");
    }

    public static CommonJsonResult fail(ExceptionEnum exceptionEnum) {
        return new CommonJsonResult(false, exceptionEnum.getKey(), exceptionEnum.getValue());
    }

    public CommonJsonResult data(Object o) {
        this.data = o;
        return this;
    }

    @SuppressWarnings("unchecked")
    public CommonJsonResult putData(String key, Object value) {
        if (null == this.data) {
            this.data = new HashMap<String, Object>();
        }
        if (this.data instanceof Map) {
            Map<String, Object> map = ((Map) this.data);
            map.put(key, value);
        }
        return this;
    }
}
