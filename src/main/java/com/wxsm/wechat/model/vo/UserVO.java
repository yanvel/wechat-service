package com.wxsm.wechat.model.vo;

import com.wxsm.wechat.model.enums.GenderEnum;
import lombok.Data;

/**
 * Created with Yang Huan
 * Date: 2017/5/27
 * Time: 11:21
 */
@Data
public class UserVO {

    private String openId;

    private String nickname;

    private GenderEnum gender;

    private String hometown;

    private Boolean showHometown;

    private String avatarUrl;
}
