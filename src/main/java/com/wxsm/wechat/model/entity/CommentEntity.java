package com.wxsm.wechat.model.entity;

import com.wxsm.wechat.model.enums.IsDelEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
@Entity(name = "wechat_comment")
@Data
@NoArgsConstructor
public class CommentEntity {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(length = 64, nullable = false)
    private String openId;//微信id

    @Column(nullable = false, length = 50)
    private String authorName;

    @Column(nullable = false)
    private Integer contentId;

    @Column(nullable = false, length = 200)
    private String comment;

    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    private IsDelEnum isDel = IsDelEnum.NOT_DELETE;

    @Column(nullable = false)
    private Date created = new Date();

}