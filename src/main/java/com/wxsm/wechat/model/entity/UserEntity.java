package com.wxsm.wechat.model.entity;

import com.wxsm.wechat.model.enums.GenderEnum;
import com.wxsm.wechat.model.enums.IsDelEnum;
import com.wxsm.wechat.util.RandomUserInfoUtil;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
@Entity(name = "wechat_user")
@Data
@NoArgsConstructor
public class UserEntity {
    @Id
    @GeneratedValue
    private Integer id;

    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false, length = 1)
    private GenderEnum gender = GenderEnum.FEMALE;

    @Column(length = 20, nullable = false)
    private String nickname;//昵称

    @Column(length = 20)
    private String username;//用户名

    @Column(length = 64)
    private String password;//密码

    @Column(nullable = false)
    private Integer age = 0;//年龄

    @Column(length = 100)
    private String description;//简介

    @Column(length = 11)
    private String telephone;//手机

    @Column(nullable = false)
    private Integer score = 0;//积分

    @Column(length = 10, nullable = false)
    private String hometown = RandomUserInfoUtil.HOME_NAME[0];//家乡

    @Column(nullable = false)
    private Boolean showHometown = true;//是否显示位置

    @Column(length = 64, nullable = false, unique = true)
    private String openId;//微信id

    @Column(length = 64)
    private String sessionKey;

    @Column(length = 200, nullable = false)
    private String avatarUrl;//微信头像

    @Column(nullable = false)
    private Date lastLoginDate;//最近登录时间

    @Column(length = 15, nullable = false)
    private String lastLoginIp;//最近登录ip

    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    private IsDelEnum isDel = IsDelEnum.NOT_DELETE;

    @Column(nullable = false, updatable = false)
    private Date created = new Date();//创建时间

    private Date updated;//更新时间

}