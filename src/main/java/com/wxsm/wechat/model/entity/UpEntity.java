package com.wxsm.wechat.model.entity;

import com.wxsm.wechat.model.GroupPK;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

/**
 * Created with Yang Huan
 * Date: 2017/6/6
 * Time: 9:55
 */
@Entity(name = "wechat_up")
@IdClass(GroupPK.class)
@Data
public class UpEntity {

    @Id
    @Column(length = 64, nullable = false)
    private String openId;

    @Id
    @Column(nullable = false)
    private Integer contentId;


}
