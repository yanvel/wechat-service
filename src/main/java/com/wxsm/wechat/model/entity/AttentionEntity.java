package com.wxsm.wechat.model.entity;

import com.wxsm.wechat.model.GroupPK;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

/**
 * Created with Yang Huan
 * Date: 2017/6/5
 * Time: 11:06
 */
@Entity(name = "wechat_attention")
@IdClass(GroupPK.class)
@Data
@NoArgsConstructor
public class AttentionEntity implements Serializable {

    @Id
    @Column(length = 64, nullable = false)
    private String openId;//微信id

    @Id
    @Column(nullable = false)
    private Integer contentId;

    @Column(length = 64, nullable = false)
    private String authorOpenId;

    @Column(nullable = false, length = 200)
    private String description;

    @Column(length = 10, nullable = false)
    private String type;

    @Transient
    private BigInteger unreadCount = BigInteger.valueOf(0);//未读数目

    @Transient
    private String commentUserName;//最新回复人名称

    @Transient
    private String message;//最新回复内容

    @Transient
    private Integer messageId;//最新回复内容id

    @Transient
    private String date;//最新回复时间
}
