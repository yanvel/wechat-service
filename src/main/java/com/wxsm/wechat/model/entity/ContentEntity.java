package com.wxsm.wechat.model.entity;

import com.wxsm.wechat.core.exception.ServiceException;
import com.wxsm.wechat.model.enums.ContentTypeEnum;
import com.wxsm.wechat.model.enums.IsDelEnum;
import com.wxsm.wechat.model.enums.IsShowEnum;
import com.wxsm.wechat.util.JsonUtil;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;
import java.util.Date;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
@Entity(name = "wechat_content")
@Data
@NoArgsConstructor
public class ContentEntity {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(length = 64, nullable = false)
    private String openId;//微信id

    @Column(nullable = false, length = 50)
    private String authorName;

    @Column(length = 200, nullable = false)
    private String avatarUrl;//微信头像

    @Column(length = 100)
    private String title;//标题

    @Column(nullable = false, length = 10000)
    private String content;//内容

    @Column(nullable = false, length = 200)
    private String description;//简述

    @Column(length = 1000)
    private String images;

    @Column(nullable = false)
    private Integer upTimes = 0;//点赞次数

    @Column(nullable = false)
    private Integer commentTimes = 0;//评论次数

    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    private ContentTypeEnum type;

    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    private IsDelEnum isDel = IsDelEnum.NOT_DELETE;

    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    private IsShowEnum isShow = IsShowEnum.SHOW;

    @Column(nullable = false)
    private Date created = new Date();//创建时间

    @Column
    private Date updated;//更新时间

    @Column(nullable = false)
    private Double longitude;//经度

    @Column(nullable = false)
    private Double latitude;//维度

    @Transient
    private Double distance = 0.00D;

    @Transient
    private Boolean isUp = false;

    @Transient
    private Boolean showHometown = false;

    @Transient
    private String hometown;

    public String[] getImages() {
        try {
            return JsonUtil.fromJson(this.images, String[].class);
        } catch (ServiceException e) {
            return new String[]{};
        }
    }

    //分页
    public ContentEntity(Integer id, String openId, String authorName, String title, String description, String images,
                         Integer upTimes, Integer commentTimes, Double longitude, Double latitude, Date created) {
        this.id = id;
        this.openId = openId;
        this.authorName = authorName;
        this.title = title;
        this.description = description;
        this.images = images;
        this.upTimes = upTimes;
        this.commentTimes = commentTimes;
        this.longitude = longitude;
        this.latitude = latitude;
        this.created = created;
    }

    public static Selection[] pageSelections(Root<ContentEntity> root) {
        return new Selection[]{
                root.get("id"),
                root.get("openId"),
                root.get("authorName"),
                root.get("title"),
                root.get("description"),
                root.get("images"),
                root.get("upTimes"),
                root.get("commentTimes"),
                root.get("longitude"),
                root.get("latitude"),
                root.get("created")
        };
    }

}