package com.wxsm.wechat.model.entity;

import com.wxsm.wechat.model.enums.IsDelEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with Yang Huan
 * Date: 2017/5/24
 * Time: 15:12
 */
@Entity(name = "wechat_banner")
@Data
@NoArgsConstructor
public class BannerEntity {

    @Id
    @GeneratedValue
    private Integer id;//自增id

    @Column(nullable = false, length = 100)
    private String description;//banner描述

    @Column(nullable = false, length = 1000)
    private String imageUrl;//图片路径

    @Column(nullable = false, length = 100)
    private String link;//图片链接

    @Column(nullable = false, length = 2)
    private String weight;//权重值,排序用

    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    private IsDelEnum isDel = IsDelEnum.NOT_DELETE;

    @Column(nullable = false)
    private Date created = new Date();//创建时间

}
