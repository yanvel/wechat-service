package com.wxsm.wechat.model.entity;

import com.wxsm.wechat.model.enums.MessageTypeEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with Yang Huan
 * Date: 2017/6/5
 * Time: 10:30
 */
@Entity(name = "wechat_message")
@Data
@NoArgsConstructor
public class MessageEntity {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(length = 64, nullable = false)
    private String openId;

    @Column(length = 20, nullable = false)
    private String authorName;

    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    private MessageTypeEnum type;

    @Column(nullable = false)
    private Integer contentId;

    @Column(length = 64)
    private String targetOpenId;

    @Column(nullable = false, length = 200)
    private String message;

    @Column(nullable = false, updatable = false)
    private Date created = new Date();//创建时间

    @Transient
    private Long groupCount;

}
