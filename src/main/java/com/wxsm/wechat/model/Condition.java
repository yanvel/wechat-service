package com.wxsm.wechat.model;


import lombok.Data;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
@Data
public class Condition {

    private static final long serialVersionUID = 1L;

    private Integer page;
    private Integer pageSize;
    private Sort sort;
    private Integer lastId;

    public PageRequest getPageRequest() {
        if (null == this.page || this.page < 1) {
            this.page = 1;
        }
        if (null == this.pageSize || this.pageSize < 1) {
            this.pageSize = 10;
        }
        return new PageRequest(this.page - 1, this.pageSize, this.sort);
    }
}
