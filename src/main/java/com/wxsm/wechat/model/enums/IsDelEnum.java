package com.wxsm.wechat.model.enums;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
public enum IsDelEnum {

    NOT_DELETE(0, "否"), DELETE(1, "是");

    private int value;
    private String name;

    IsDelEnum(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
