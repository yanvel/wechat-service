package com.wxsm.wechat.model.enums;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
public enum MessageTypeEnum {

    CHAT(0, "聊天"),
    NEARBY_COMMENT(1, "附近评论");

    private int value;
    private String name;

    MessageTypeEnum(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public static String getName(int value) {
        for (MessageTypeEnum contentTypeEnum : values()) {
            if (contentTypeEnum.value == value) {
                return contentTypeEnum.getName();
            }
        }
        return null;
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
