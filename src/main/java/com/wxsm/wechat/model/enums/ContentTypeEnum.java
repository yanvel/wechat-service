package com.wxsm.wechat.model.enums;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
public enum ContentTypeEnum {

    NEWS(0, "新闻"),
    NEARBY(1, "附近"),
    COMMON_PHONE(2, "常用电话"),
    LIFE_PAGES(3, "生活黄页"),
    SHOP(4, "店铺");

    private int value;
    private String name;

    ContentTypeEnum(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public static String getName(int value) {
        for (ContentTypeEnum contentTypeEnum : values()) {
            if (contentTypeEnum.value == value) {
                return contentTypeEnum.getName();
            }
        }
        return null;
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
