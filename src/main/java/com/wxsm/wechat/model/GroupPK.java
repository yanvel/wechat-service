package com.wxsm.wechat.model;

import lombok.Data;

import java.io.Serializable;

/**
 * Created with Yang Huan
 * Date: 2017/6/6
 * Time: 10:53
 */
@Data
public class GroupPK implements Serializable {

    private String openId;

    private Integer contentId;

}