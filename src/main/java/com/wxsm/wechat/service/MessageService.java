package com.wxsm.wechat.service;

import com.wxsm.wechat.core.socket.WebSocketService;
import com.wxsm.wechat.dao.AttentionDao;
import com.wxsm.wechat.dao.CommentDao;
import com.wxsm.wechat.dao.ContentDao;
import com.wxsm.wechat.dao.MessageDao;
import com.wxsm.wechat.model.entity.AttentionEntity;
import com.wxsm.wechat.model.entity.CommentEntity;
import com.wxsm.wechat.model.entity.ContentEntity;
import com.wxsm.wechat.util.ActionUtil;
import com.wxsm.wechat.util.DateUtil;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
@Service
@AllArgsConstructor
public class MessageService {

    private final MessageDao messageDao;
    private final ContentDao contentDao;
    private final CommentDao commentDao;
    private final AttentionDao attentionDao;
    private final WebSocketService webSocketService;

    public List<AttentionEntity> findAllAttention() {
        List<AttentionEntity> attentionList = attentionDao.findByOpenId(ActionUtil.getOpenId());
        messageDao.findNearbyOfflineMessage(ActionUtil.getOpenId()).forEach(messageEntity -> attentionList.forEach(attentionEntity -> {
            if (messageEntity[0].equals(attentionEntity.getContentId())) {
                attentionEntity.setCommentUserName((String) messageEntity[1]);
                attentionEntity.setDate(DateUtil.toString((Date) messageEntity[2], "HH:mm"));
                attentionEntity.setUnreadCount((BigInteger) messageEntity[3]);
                attentionEntity.setMessageId((Integer) messageEntity[4]);
                attentionEntity.setMessage((String) messageEntity[5]);
            }
        }));
        return attentionList;
    }

    @Transactional
    public CommentEntity comment(CommentEntity commentEntity) {
        this.attention(commentEntity.getContentId());
        contentDao.addCommentTimes(commentEntity.getContentId());
        webSocketService.send(commentEntity);
        return commentDao.save(commentEntity);
    }

    @Transactional
    public void cancelAttention(Integer contentId) {
        attentionDao.cancelAttention(ActionUtil.getOpenId(), contentId);
    }

    public void attention(Integer contentId) {
        ContentEntity contentEntity = contentDao.findOne(contentId);
        AttentionEntity attentionEntity = new AttentionEntity();
        attentionEntity.setOpenId(ActionUtil.getOpenId());
        attentionEntity.setType(contentEntity.getType().getName());
        attentionEntity.setContentId(contentId);
        attentionEntity.setAuthorOpenId(contentEntity.getOpenId());
        attentionEntity.setDescription(contentEntity.getDescription());
        attentionDao.save(attentionEntity);
    }

    @Async
    @Transactional
    public void removeOfflineMessage(Integer contentId, String openId) {
        messageDao.removeOfflineMessage(openId, contentId);
    }

    public AttentionEntity findAttention(Integer contentId) {
        return attentionDao.findByOpenIdAndContentId(ActionUtil.getOpenId(), contentId);
    }

}
