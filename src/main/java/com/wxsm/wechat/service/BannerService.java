package com.wxsm.wechat.service;

import com.wxsm.wechat.dao.BannerDao;
import com.wxsm.wechat.model.entity.BannerEntity;
import com.wxsm.wechat.model.enums.IsDelEnum;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
@Service
@AllArgsConstructor
public class BannerService {

    private final BannerDao bannerDao;

    public List<BannerEntity> findAll() {
        return bannerDao.findByIsDelOrderByWeightAsc(IsDelEnum.NOT_DELETE);
    }
}
