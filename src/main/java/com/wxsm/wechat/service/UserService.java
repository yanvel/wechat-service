package com.wxsm.wechat.service;

import com.wxsm.wechat.core.exception.ExceptionEnum;
import com.wxsm.wechat.core.exception.ServiceException;
import com.wxsm.wechat.core.rest.MyRestClient;
import com.wxsm.wechat.dao.UserDao;
import com.wxsm.wechat.model.entity.UserEntity;
import com.wxsm.wechat.model.enums.GenderEnum;
import com.wxsm.wechat.model.vo.UserVO;
import com.wxsm.wechat.util.ActionUtil;
import com.wxsm.wechat.util.CommonUtil;
import com.wxsm.wechat.util.IPUtil;
import com.wxsm.wechat.util.RandomUserInfoUtil;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.Map;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
@Service
@AllArgsConstructor
public class UserService {

    private final UserDao userDao;
    private final MyRestClient myRestClient;
    private static final String URL = "https://api.weixin.qq.com/sns/jscode2session?appid=wx40fdc3d7687a99a8&secret=bef12f6f0d61f879e73533f52c5de371&js_code=%s&grant_type=authorization_code";

    public UserEntity login(String code) throws ServiceException {
        String openId = this.getOpenId(code);
        UserEntity userEntity = userDao.findByOpenId(openId);
        if (null != userEntity) {
            userEntity.setLastLoginDate(new Date());
            userEntity.setLastLoginIp(IPUtil.getClientIp());
            userDao.save(userEntity);
        } else {
            userEntity = new UserEntity();
            userEntity.setOpenId(openId);
        }
        return userEntity;
    }

    public int countByOpenId(String openId) {
        return userDao.countByOpenId(openId);
    }

    public UserEntity create(UserVO userVO) {
        UserEntity userEntity = new UserEntity();
        userEntity.setOpenId(userVO.getOpenId());
        userEntity.setLastLoginDate(new Date());
        userEntity.setLastLoginIp(IPUtil.getClientIp());
        userEntity.setNickname(StringUtils.isEmpty(userVO.getNickname()) ? RandomUserInfoUtil.getChineseName() : userVO.getNickname());
        userEntity.setAvatarUrl(StringUtils.isEmpty(userVO.getAvatarUrl()) ? RandomUserInfoUtil.getHead() : userVO.getAvatarUrl());
        userEntity.setGender(null == userVO.getGender() ? GenderEnum.FEMALE : userVO.getGender());
        userDao.save(userEntity);
        return userEntity;
    }

    public UserEntity update(UserVO userVO) {
        UserEntity userEntity = userDao.findByOpenId(ActionUtil.getOpenId());
        BeanUtils.copyProperties(userVO, userEntity, CommonUtil.getNullPropertyNames(userVO));
        userEntity.setUpdated(new Date());
        return userDao.save(userEntity);
    }

    private String getOpenId(String code) throws ServiceException {
        Map<String, String> result = myRestClient.getForMap(String.format(URL, code));
        if (result.containsKey("errcode")) {
            throw new ServiceException(ExceptionEnum.LOGIN_FAIL);
        }
        return result.get("openid");
    }

}
