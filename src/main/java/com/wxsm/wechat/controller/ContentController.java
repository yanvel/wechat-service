package com.wxsm.wechat.controller;

import com.wxsm.wechat.core.aop.NeedLogin;
import com.wxsm.wechat.model.CommonJsonResult;
import com.wxsm.wechat.model.Condition;
import com.wxsm.wechat.model.entity.BannerEntity;
import com.wxsm.wechat.model.entity.CommentEntity;
import com.wxsm.wechat.model.entity.ContentEntity;
import com.wxsm.wechat.service.BannerService;
import com.wxsm.wechat.service.ContentService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 内容相关操作
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
@Controller
@RequestMapping(value = "/content")
@AllArgsConstructor
public class ContentController {

    private final ContentService contentService;

    private final BannerService bannerService;

    /**
     * 查询首页
     *
     * @param condition
     * @return
     */
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    @ResponseBody
    public CommonJsonResult index(Condition condition) {
        Page<ContentEntity> newsPage = contentService.findNewsContentPage(condition.getPageRequest());
        List<BannerEntity> banners = bannerService.findAll();
        return CommonJsonResult.success().putData("news", newsPage.getContent()).putData("hasNext", newsPage.hasNext()).putData("banners", banners);
    }

    /**
     * 分页查询新闻
     *
     * @param condition
     * @return
     */
    @RequestMapping(value = "/news", method = RequestMethod.GET)
    @ResponseBody
    public CommonJsonResult news(Condition condition) {
        Page<ContentEntity> newsPage = contentService.findNewsContentPage(condition.getPageRequest());
        return CommonJsonResult.success().putData("news", newsPage.getContent()).putData("hasNext", newsPage.hasNext());
    }

    /**
     * 获取附近发布
     *
     * @param longitude
     * @param latitude
     * @return
     */
    @RequestMapping(value = "/nearby", method = RequestMethod.GET)
    @ResponseBody
    public CommonJsonResult findNearby(Double longitude, Double latitude) {
        List<ContentEntity> list = contentService.findNearbyContent(longitude, latitude);
        return CommonJsonResult.success().data(list);
    }

    /**
     * 查询发布详情
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/nearby/{id}", method = RequestMethod.GET)
    @ResponseBody
    public CommonJsonResult detail(@PathVariable Integer id, Double longitude, Double latitude, Condition condition) {
        ContentEntity contentEntity = contentService.findNearbyContentById(id, longitude, latitude);
        Page<CommentEntity> commentsPage = contentService.findCommentsPage(id, condition);
        return CommonJsonResult.success().putData("content", contentEntity).putData("comments", commentsPage.getContent()).putData("hasNext", commentsPage.hasNext());
    }

    /**
     * 分页查询发布详情的评论
     *
     * @param contentId
     * @param condition
     * @return
     */
    @RequestMapping(value = "/nearby/comments/{contentId}", method = RequestMethod.GET)
    @ResponseBody
    public CommonJsonResult comments(@PathVariable Integer contentId, Condition condition) {
        Page<CommentEntity> commentsPage = contentService.findCommentsPage(contentId, condition);
        return CommonJsonResult.success().putData("comments", commentsPage.getContent()).putData("hasNext", commentsPage.hasNext());
    }

    /**
     * 发布附近
     *
     * @param contentEntity
     * @return
     */
    @RequestMapping(value = "/nearby", method = RequestMethod.POST)
    @ResponseBody
    @NeedLogin
    public CommonJsonResult create(@RequestBody ContentEntity contentEntity) {
        ContentEntity result = contentService.saveNearbyContent(contentEntity);
        return CommonJsonResult.success().data(result);
    }

    /**
     * 点赞
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/up/{id}", method = RequestMethod.PUT)
    @ResponseBody
    @NeedLogin
    public CommonJsonResult up(@PathVariable Integer id) {
        contentService.up(id);
        return CommonJsonResult.success();
    }


    /**
     * 删除
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/nearby/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    @NeedLogin
    public CommonJsonResult delete(@PathVariable Integer id) {
        contentService.delete(id);
        return CommonJsonResult.success();
    }
}
