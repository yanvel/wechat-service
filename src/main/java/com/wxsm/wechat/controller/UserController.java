package com.wxsm.wechat.controller;

import com.wxsm.wechat.core.aop.NeedLogin;
import com.wxsm.wechat.core.exception.ServiceException;
import com.wxsm.wechat.model.CommonJsonResult;
import com.wxsm.wechat.model.entity.UserEntity;
import com.wxsm.wechat.model.vo.UserVO;
import com.wxsm.wechat.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 用户相关操作
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
@Controller
@RequestMapping(value = "/user")
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    /**
     * 登录
     *
     * @param code
     * @return
     */
    @RequestMapping(value = "/login/{code}", method = RequestMethod.GET)
    @ResponseBody
    public CommonJsonResult login(@PathVariable String code) throws ServiceException {
        UserEntity userEntity = userService.login(code);
        return CommonJsonResult.success().data(userEntity);
    }

    /**
     * 创建用户
     *
     * @param userVO
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public CommonJsonResult create(@RequestBody UserVO userVO) {
        UserEntity userEntity = userService.create(userVO);
        return CommonJsonResult.success().data(userEntity);
    }

    /**
     * 更新用户信息
     *
     * @param userVO
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.PUT)
    @ResponseBody
    @NeedLogin
    public CommonJsonResult update(@RequestBody UserVO userVO) {
        UserEntity userEntity = userService.update(userVO);
        return CommonJsonResult.success().data(userEntity);
    }
}
