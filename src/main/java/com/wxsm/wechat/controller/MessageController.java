package com.wxsm.wechat.controller;

import com.wxsm.wechat.core.aop.NeedLogin;
import com.wxsm.wechat.model.CommonJsonResult;
import com.wxsm.wechat.model.entity.AttentionEntity;
import com.wxsm.wechat.model.entity.CommentEntity;
import com.wxsm.wechat.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 消息相关操作
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
@Controller
@RequestMapping(value = "/message")
@AllArgsConstructor
public class MessageController {

    private final MessageService messageService;

    /**
     * 查询关注列表
     *
     * @return
     */
    @RequestMapping(value = "/attention", method = RequestMethod.GET)
    @ResponseBody
    @NeedLogin
    public CommonJsonResult attention() {
        List<AttentionEntity> list = messageService.findAllAttention();
        return CommonJsonResult.success().data(list);
    }

    /**
     * 回复
     *
     * @param commentEntity
     * @return
     */
    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    @ResponseBody
    @NeedLogin
    public CommonJsonResult comment(@RequestBody CommentEntity commentEntity) {
        CommentEntity result = messageService.comment(commentEntity);
        return CommonJsonResult.success().data(result);
    }

    /**
     * 关注
     *
     * @param contentId
     * @return
     */
    @RequestMapping(value = "/attention/{contentId}", method = RequestMethod.PUT)
    @ResponseBody
    @NeedLogin
    public CommonJsonResult attention(@PathVariable Integer contentId) {
        messageService.attention(contentId);
        return CommonJsonResult.success();
    }

    /**
     * 取消关注
     *
     * @param contentId
     * @return
     */
    @RequestMapping(value = "/attention/{contentId}", method = RequestMethod.DELETE)
    @ResponseBody
    @NeedLogin
    public CommonJsonResult cancenAttention(@PathVariable Integer contentId) {
        messageService.cancelAttention(contentId);
        return CommonJsonResult.success();
    }

    /**
     * 根据contentId查询当前用户的关注
     *
     * @param contentId
     * @return
     */
    @RequestMapping(value = "/attention/{contentId}", method = RequestMethod.GET)
    @ResponseBody
    @NeedLogin
    public CommonJsonResult findAttention(@PathVariable Integer contentId) {
        AttentionEntity attentionEntity = messageService.findAttention(contentId);
        return CommonJsonResult.success().data(attentionEntity);
    }
}
