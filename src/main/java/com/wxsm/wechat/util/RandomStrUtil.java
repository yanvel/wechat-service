package com.wxsm.wechat.util;

/**
 * Created with Yang Huan
 * Date: 2017/5/17
 * Time: 10:53
 */
public class RandomStrUtil {

    private static String RANDOM_STR = "ABCDEFGHIJKLKMOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    private static String RANDOM_NUM = "0123456789";

    public static String getRandomString(int length) {
        return getRandom(length, RANDOM_STR);
    }

    public static String getRandomNum(int length) {
        return getRandom(length, RANDOM_NUM);
    }

    public static String getRandom(int length, String randomChar) {
        StringBuffer sb = new StringBuffer();
        int len = randomChar.length();
        for (int i = 0; i < length; i++) {
            char randomCode = getOneRandomCode(randomChar, len);
            sb.append(randomCode);
        }
        return sb.toString();
    }

    public static String getOneRandomStringCode() {
        return String.valueOf(getOneRandomCode(RANDOM_STR, RANDOM_STR.length()));
    }

    private static char getOneRandomCode(String randomChar, int len) {
        return randomChar.charAt(getRandomIndex(len - 1));
    }

    private static int getRandomIndex(int count) {
        return (int) Math.round(Math.random() * (count));
    }

}
