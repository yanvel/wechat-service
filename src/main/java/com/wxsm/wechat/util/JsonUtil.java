package com.wxsm.wechat.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.wxsm.wechat.core.exception.ExceptionEnum;
import com.wxsm.wechat.core.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
public class JsonUtil {

    private static Logger logger = Logger.getLogger(JsonUtil.class);

    private static ObjectMapper mapper = null;

    static {
        mapper = new ObjectMapper();
        // 设置输入时忽略在JSON字符串中存在但Java对象实际没有的属性
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        // 允许key没有使用双引号的json
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
        mapper.setDateFormat(format);
        // 驼峰转换
        mapper.setPropertyNamingStrategy(PropertyNamingStrategy.LOWER_CAMEL_CASE);
        //配置不写value为null的key
        mapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
        // 输出格式化
        mapper.configure(SerializationFeature.INDENT_OUTPUT, false);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    public static String toJson(Object obj) throws ServiceException {
        Assert.notNull(obj, "序列化参数obj不能为空");
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new ServiceException(ExceptionEnum.JSON_FORMAT_ERROR, e.getMessage());
        }
    }

    public static byte[] toBytes(Object obj) throws ServiceException {
        Assert.notNull(obj, "序列化参数obj不能为空");
        try {
            return mapper.writeValueAsBytes(obj);
        } catch (JsonProcessingException e) {
            throw new ServiceException(ExceptionEnum.JSON_FORMAT_ERROR, e.getMessage());
        }
    }

    public static String toJson(Object key, Object value) throws ServiceException {
        Assert.notNull(key, "序列化参数key不能为空");
        Map<Object, Object> map = new HashMap<>();
        map.put(key, value);
        try {
            return mapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            throw new ServiceException(ExceptionEnum.JSON_FORMAT_ERROR, e.getMessage());
        }
    }

    public static <T> T fromJson(String jsonString, Class<T> clazz) throws ServiceException {
        Assert.hasText(jsonString, "序列化参数jsonString不能为空");
        Assert.notNull(clazz, "clazz不能为空");
        try {
            return mapper.readValue(jsonString, clazz);
        } catch (IOException e) {
            throw new ServiceException(ExceptionEnum.JSON_FORMAT_ERROR, e.getMessage());
        }
    }

    public static <L extends Collection<E>, E> L fromJson(String jsonString, Class<L> collectionClass, Class<E> elementClass) throws ServiceException {
        Assert.hasText(jsonString, "序列化参数jsonString不能为空");
        Assert.notNull(collectionClass, "collectionClass不能为空");
        Assert.notNull(elementClass, "elementClass不能为空");
        CollectionType type = mapper.getTypeFactory().constructCollectionType(collectionClass, elementClass);
        try {
            return mapper.readValue(jsonString, type);
        } catch (IOException e) {
            throw new ServiceException(ExceptionEnum.JSON_FORMAT_ERROR, e.getMessage());
        }
    }

    public static <T> T fromJson(String jsonString, TypeReference<T> typeRef) throws ServiceException {
        Assert.hasText(jsonString, "序列化参数jsonString不能为空");
        Assert.notNull(typeRef, "typeRef不能为空");
        try {
            return mapper.readValue(jsonString, typeRef);
        } catch (IOException e) {
            throw new ServiceException(ExceptionEnum.JSON_FORMAT_ERROR, e.getMessage());
        }
    }

    public static String toJsonP(String functionName, Object object) throws ServiceException {
        Assert.hasText(functionName, "序列化参数functionName不能为空");
        Assert.notNull(object, "序列化参数object不能为空");
        return toJson(new JSONPObject(functionName, object));
    }

    public static <T> T convertValue(Object object, Class<T> clazz) {
        Assert.notNull(object, "序列化参数object不能为空");
        Assert.notNull(clazz, "clazz不能为空");
        return mapper.convertValue(object, clazz);
    }

}
