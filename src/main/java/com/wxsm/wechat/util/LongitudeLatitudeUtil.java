package com.wxsm.wechat.util;

import lombok.Data;

/**
 * 根据坐标计算距离的工具类，推荐使用spatial4j的工具类
 * Created by yanvel on 2017/5/28.
 */
@Data
@Deprecated
public class LongitudeLatitudeUtil {

    private static final Double EARTH_RADIUS = 6378137D;//米

    public static final Double DEFAULT_DISTANCE = 100 * 1000D;

    private Double latSourceRad;

    private Double latSourceRadCos;

    private Double minX;

    private Double maxX;

    private Double lngSourceRad;

    private Double minY;

    private Double maxY;

    public LongitudeLatitudeUtil(Double lngSource, Double latSource) {
        this.latSourceRad = Math.toRadians(latSource);
        this.lngSourceRad = Math.toRadians(lngSource);
        this.latSourceRadCos = Math.cos(this.latSourceRad);
        Double distance = DEFAULT_DISTANCE;
        Double dpmLng = Math.toDegrees(2 * Math.asin(Math.sin(distance / (2 * EARTH_RADIUS)) / Math.cos(latSource)));
        this.minX = lngSource - dpmLng;
        this.maxX = lngSource + dpmLng;
        Double dpmLat = Math.toDegrees(distance / EARTH_RADIUS);
        this.minY = latSource - dpmLat;
        this.maxY = latSource + dpmLat;
    }

    public Double getDistance(Double lngTarget, Double latTarget) {
        double latTargetRad = Math.toRadians(latTarget);
        double lngTargetRed = Math.toRadians(lngTarget);
        double a = Math.abs(this.latSourceRad - latTargetRad);
        double n = Math.abs(this.lngSourceRad - lngTargetRed);
        double distance = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + latSourceRadCos * Math.cos(latTargetRad) * Math.pow(Math.sin(n / 2), 2)));
        distance = distance * EARTH_RADIUS;
        distance = Math.round(distance * 10000d) / 10000d;
        return distance;
    }

}
