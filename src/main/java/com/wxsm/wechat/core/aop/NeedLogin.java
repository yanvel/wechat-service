package com.wxsm.wechat.core.aop;

import java.lang.annotation.*;

/**
 * Created by yanghuan on 2016/10/28.
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NeedLogin {

    boolean needLogin() default true;

}