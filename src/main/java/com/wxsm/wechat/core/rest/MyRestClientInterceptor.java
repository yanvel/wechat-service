package com.wxsm.wechat.core.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.support.HttpRequestWrapper;

import java.io.IOException;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
public class MyRestClientInterceptor implements ClientHttpRequestInterceptor {

    public static final Logger logger = LoggerFactory.getLogger(MyRestClientInterceptor.class);

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        HttpRequestWrapper wrapper = new HttpRequestWrapper(request);
        HttpHeaders headers = wrapper.getHeaders();
        headers.set("Content-Type", "application/json;charset=utf-8");
        logger.info("rest请求method:{}", request.getMethod());
        logger.info("rest请求URL:{}", request.getURI().toString());
        logger.info("rest请求body:{}", new String(body));
        return execution.execute(wrapper, body);
    }
}
