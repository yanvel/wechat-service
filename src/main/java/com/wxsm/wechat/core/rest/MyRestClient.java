package com.wxsm.wechat.core.rest;

import com.wxsm.wechat.core.exception.ServiceException;

import java.util.List;
import java.util.Map;

/**
 * rest封装
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
public interface MyRestClient {

    Map getForMap(String url) throws ServiceException;

    <T> T getForObject(String url, Class<T> elementType);

    <T> T getForObject(String url, Class<T> elementType, Object urlVariables);

    <T> T getForObject(String url, Class<T> elementType, Map<String, ?> urlVariables);

    <T> List<T> getForList(String url, Class<T> elementType) throws ServiceException;

    <T> List<T> getForList(String url, Class<T> elementType, Object urlVariables) throws ServiceException;

    <T> List<T> getForList(String url, Class<T> elementType, Map<String, ?> urlVariables) throws ServiceException;

    String postForObject(String url, Object request);

    <T> T postForObject(String url, Object request, Class<T> responseType);

    <T> T patchForObject(String url, Object request, Class<T> responseType);

    <T> T delete(String url, Class<T> responseType);

}