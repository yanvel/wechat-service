package com.wxsm.wechat.core.jpa;

import lombok.Builder;
import lombok.Getter;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Selection;
import java.util.List;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
@Builder
@Getter
public class JpaSelection {

    private List<Predicate> predicates;
    private Selection[] selections;
    private Expression[] expressions;
    private Order[] orders;

}
