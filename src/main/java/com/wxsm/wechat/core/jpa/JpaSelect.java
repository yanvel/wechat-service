package com.wxsm.wechat.core.jpa;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
public interface JpaSelect<T> {

    JpaSelection getSelection(Root<T> root, CriteriaBuilder criteriaBuilder);
}
