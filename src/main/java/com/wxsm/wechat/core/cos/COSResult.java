package com.wxsm.wechat.core.cos;

import lombok.Data;

import java.util.Map;

/**
 * Created with Yang Huan
 * Date: 2017/5/26
 * Time: 12:41
 */
@Data
public class COSResult {
    private Integer code;
    private String message;
    private String request_id;
    private Map<String, Object> data;

    public Boolean isSuccess() {
        return this.code == 0;
    }

}