package com.wxsm.wechat.core.cos;

import com.wxsm.wechat.core.exception.ServiceException;

/**
 * Created with Yang Huan
 * Date: 2017/5/26
 * Time: 11:39
 */
public interface MyCOSClient {

    COSResult uploadFile(String cosPath, byte[] contentBuffer) throws ServiceException;

    COSResult delFile(String cosPath) throws ServiceException;

    COSResult listFolder(String cosPath) throws ServiceException;

    void delFolder(String cosPath) throws ServiceException;

}
