package com.wxsm.wechat.core.cos;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.sign.Credentials;

/**
 * Created with Yang Huan
 * Date: 2017/5/26
 * Time: 11:39
 */
public class COSClientUtil {

    private static COSClient cosClient;
    private static final Long APP_ID = 1251421985L;
    private static final String SECRET_ID = "AKIDRhjX6UtYEfroVzKF4ez5wZx60f5FwnCL";
    private static final String SECRET_KEY = "2pMhsoVyCBvRjlZ9TXLtBDzVlma9YjgM";

    /**
     * 获取cos实例
     *
     * @return
     */
    public static COSClient getInstance() {
        if (null == cosClient) {
            Credentials credentials = new Credentials(APP_ID, SECRET_ID, SECRET_KEY);
            ClientConfig clientConfig = new ClientConfig();
            clientConfig.setRegion("sh");//设置bucket所在的区域，比如华南园区：gz； 华北园区：tj；华东园区：sh
            cosClient = new COSClient(clientConfig, credentials);
        }
        return cosClient;
    }

}
