package com.wxsm.wechat.core.exception;

import lombok.Getter;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
@Getter
public class ServiceException extends Exception {

    private String code;

    private ServiceException(String key, String message) {
        super(message);
        this.code = key;
    }

    public ServiceException(ExceptionEnum exceptionEnum) {
        this(exceptionEnum.getKey(), exceptionEnum.getValue());
    }

    public ServiceException(ExceptionEnum exceptionEnum, String message) {
        this(exceptionEnum.getKey(), message);
    }

}
