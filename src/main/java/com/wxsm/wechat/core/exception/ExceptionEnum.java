package com.wxsm.wechat.core.exception;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
public enum ExceptionEnum {

    SYS_ERROR("SYS_ERROR", "系统异常，请联系管理员"),
    DATE_FORMAT_ERROR("DATE_FORMAT_ERROR", "日期转换错误"),
    REQUEST_DATA_ERROR("REQUEST_DATA_ERROR", "请求参数异常"),
    SQL_EXECUTE_ERROR("SQL_EXECUTE_ERROR", "SQL语句执行异常"),
    LOGIN_FAIL("LOGIN_FAIL", "登录失败，请重新登录"),
    JSON_FORMAT_ERROR("JSON_FORMAT_ERROR", "json转换错误"),
    UPLOAD_ERROR("UPLOAD_ERROR", "上传文件异常"),
    NOT_LOGIN("NOT_LOGIN", "未登录");

    private String key;
    private String value;

    ExceptionEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static ExceptionEnum valueOfRestException(ServiceException restException) {
        ExceptionEnum[] exceptionEnums = ExceptionEnum.values();
        for (ExceptionEnum exceptionEnum : exceptionEnums) {
            if (exceptionEnum.getKey().equals(restException.getCode())) {
                return exceptionEnum.replaceValue(restException.getMessage());
            }
        }
        return null;
    }

    public ExceptionEnum replaceValue(String value) {
        this.setValue(value);
        return this;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
