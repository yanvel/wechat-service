package com.wxsm.wechat.core.socket;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

/**
 * Created with Yang Huan
 * Date: 2017/6/5
 * Time: 10:07
 */
@Component
@AllArgsConstructor
public class WebSocketHandler extends TextWebSocketHandler {

    private final WebSocketService webSocketService;

    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        webSocketService.online(session);
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) {
        webSocketService.send(session, message);
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable ex) {
        webSocketService.error(session, ex);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        webSocketService.offline(session);
    }

}
