package com.wxsm.wechat.dao;

import com.wxsm.wechat.core.jpa.MyRepository;
import com.wxsm.wechat.model.entity.UserEntity;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
public interface UserDao extends MyRepository<UserEntity, Integer> {

    UserEntity findByUsername(String username);

    UserEntity findByOpenId(String openId);

    int countByOpenId(String openId);
}
