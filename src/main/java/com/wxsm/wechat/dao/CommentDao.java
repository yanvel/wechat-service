package com.wxsm.wechat.dao;

import com.wxsm.wechat.core.jpa.MyRepository;
import com.wxsm.wechat.model.entity.CommentEntity;
import com.wxsm.wechat.model.enums.IsDelEnum;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
public interface CommentDao extends MyRepository<CommentEntity, Integer> {

    List<CommentEntity> findByContentIdAndIsDelOrderByCreatedDesc(Integer contentId, IsDelEnum isDel, Pageable pageable);
}
