
package com.wxsm.wechat.dao;

import com.wxsm.wechat.core.jpa.MyRepository;
import com.wxsm.wechat.model.entity.MessageEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
public interface MessageDao extends MyRepository<MessageEntity, Integer> {

    String findNearbyOfflineMessageSql = "select a.content_id, a.author_name, max(a.created), count(a.content_id), max(a.id), (select message from wechat_message where id = max(a.id)) from wechat_message a where a.target_open_id = ?1 and a.type = 1 group by a.content_id";

    //如果返回值与实体类不对应，则返回Object，否则抛异常
    @Query(value = findNearbyOfflineMessageSql, nativeQuery = true)
    List<Object[]> findNearbyOfflineMessage(String targetOpenId);

    @Modifying(clearAutomatically = true)
    @Query("DELETE FROM wechat_message message WHERE message.openId = ?1 AND message.contentId = ?2")
    void removeOfflineMessage(String openId, Integer contentId);
}
