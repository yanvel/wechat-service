package com.wxsm.wechat.dao;

import com.wxsm.wechat.core.jpa.MyRepository;
import com.wxsm.wechat.model.entity.ContentEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
public interface ContentDao extends MyRepository<ContentEntity, Integer> {

    @Modifying(clearAutomatically = true)
    @Query("UPDATE wechat_content content SET content.commentTimes = content.commentTimes + 1 WHERE content.id = ?1")
    void addCommentTimes(Integer id);


    @Modifying(clearAutomatically = true)
    @Query("UPDATE wechat_content content SET content.upTimes = content.upTimes + 1 WHERE content.id = ?1")
    void up(Integer id);

    @Modifying(clearAutomatically = true)
    @Query("UPDATE wechat_content content SET content.isDel = 1 WHERE content.id = ?1")
    void delete(Integer id);

    @Modifying(clearAutomatically = true)
    @Query("UPDATE wechat_content content SET content.isShow = 0 WHERE content.openId = ?1 AND content.type = 1 AND content.isShow = 1")
    void updateNearbyContentNotShow(String openId);
}
