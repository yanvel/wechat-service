package com.wxsm.wechat.dao;

import com.wxsm.wechat.core.jpa.MyRepository;
import com.wxsm.wechat.model.entity.AttentionEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
public interface AttentionDao extends MyRepository<AttentionEntity, Integer> {

    List<AttentionEntity> findByOpenId(String openId);

    List<AttentionEntity> findByContentId(Integer contentId);

    AttentionEntity findByOpenIdAndContentId(String openId, Integer contentId);

    @Modifying(clearAutomatically = true)
    @Query("DELETE FROM wechat_attention attention WHERE attention.openId = ?1 AND attention.contentId = ?2")
    void cancelAttention(String openId, Integer contentId);

}
