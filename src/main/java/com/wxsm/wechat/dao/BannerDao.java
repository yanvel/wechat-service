
package com.wxsm.wechat.dao;

import com.wxsm.wechat.core.jpa.MyRepository;
import com.wxsm.wechat.model.entity.BannerEntity;
import com.wxsm.wechat.model.enums.IsDelEnum;

import java.util.List;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
public interface BannerDao extends MyRepository<BannerEntity, Integer> {

    List<BannerEntity> findByIsDelOrderByWeightAsc(IsDelEnum isDelEnum);
}
