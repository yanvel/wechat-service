package com.wxsm.wechat.dao;

import com.wxsm.wechat.core.jpa.MyRepository;
import com.wxsm.wechat.model.entity.UpEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created with Yang Huan
 * Date: 2017/3/2
 * Time: 11:34
 */
public interface UpDao extends MyRepository<UpEntity, Integer> {

    @Query("SELECT up.contentId FROM wechat_up up WHERE up.openId = ?1 AND up.contentId IN ?2")
    List<Integer> selectUpContentIds(String openId, List<Integer> contentIds);

    int countByOpenIdAndContentId(String openId, Integer contentId);

    @Modifying(clearAutomatically = true)
    @Query(value = "INSERT INTO wechat_up(open_id,content_id) VALUES(?1, ?2)", nativeQuery = true)
    void insert(String openId, Integer contentId);
}
