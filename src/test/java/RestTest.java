import com.wxsm.wechat.core.rest.MyRestClient;
import com.wxsm.wechat.core.rest.MyRestClientImpl;
import com.wxsm.wechat.model.entity.UserEntity;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yanvel on 2017/6/13.
 */
public class RestTest {

    @Test
    public void updateUser() {
        MyRestClient myRestClient = new MyRestClientImpl();
        Map map = new HashMap();
        map.put("nickname", "sssss");
        UserEntity userEntity = myRestClient.patchForObject("https://www.5x4m.com/user", map, UserEntity.class);
        System.out.println(userEntity);
    }
}
