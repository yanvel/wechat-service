import com.wxsm.wechat.Application;
import com.wxsm.wechat.model.entity.ContentEntity;
import com.wxsm.wechat.service.ContentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

/**
 * @author yanghuan
 * @date 2016年8月09日
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class APITest {

    @Autowired
    private ContentService contentService;

    @Test
    public void login() {
        List<ContentEntity> list = contentService.findNearbyContent(115.70431, 30.00901);
        System.out.println(list);
    }

    @Test
    public void up() {
        contentService.up(1);
    }

}
